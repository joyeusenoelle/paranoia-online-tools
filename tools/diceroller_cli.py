#- 
# Import random, der
#
from random import *

#-
# Initialize the dice_result list
#
dice_result = []

#-
# Initialize counters for dice values ( >5< )
#
num_not_success = 0
num_success = 0

#-
# Get the players NODE
#
print( "What is your NODE Citizen?" )
NODE = int( input() )

if NODE > 0:
	print( "Excellent work Citizen, your NODE means that you cannot reasonably fail.", NODE )
elif NODE == 0:
	print( "Would you like some help Citizen?", NODE )
else:
	print( "Citizen, Please report to R&D for a MANDATORY UP-GRADE to your Cerebral CoreTech.", NODE )

#-
# Get absolute value of NODE, and roll dice :)
#
NODE_count = abs ( NODE )

while NODE_count > 0 :
    dice_result.append( int( randint(1, 6) ))
    NODE_count -= 1

#-
# Lets get Friend Computer involved! :D
#

computer_dice = int( randint(1, 6))
if computer_dice > 5:
	num_success += 1
else:
	num_not_success += 1


#-
# Count number of successes, and non-successes
# That way we can do math later, if NODE is negative
#

num_not_success += sum(map(lambda b: 1 if b < 5 else 0, dice_result))
num_success += sum(map(lambda b: 1 if b >= 5 else 0, dice_result))

print( "You rolled:", dice_result )

if NODE > 0:
    outcome = num_success - num_not_success
else:
    outcome = num_success

if computer_dice < 6:
	print( "Computer Dice is: ", computer_dice)
else:
	print( "Computer Dice is: COMPUTER")

print( "Number of Not Successes: ", num_not_success )
print( "Number of Successes: ", num_success )
print( "The outcome is:", outcome )
